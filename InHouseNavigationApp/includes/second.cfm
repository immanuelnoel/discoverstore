<cfclient>
	<cfset addListenersUpdate()>
	
	<cffunction access="public" name="compassDetectionStatus" returntype="void" >
		<cfargument name="result" required="yes">
		<cfset createSourcePointer(#result.magneticHeading#)>
	</cffunction>
	
	<cffunction access="public" name="tagDetectedUpdate" returntype="void">
		<cfargument name="nfcEvent" required="yes">
		<!--- <cfset NFCString = JSON.stringify(nfc.bytesToHexString(nfcEvent.tag.id))>
		<cfset var dbServ = new server.webservice()>
		<cfset NFCVertex = dbServ.getVertexFromNFC(NFCString)> --->
		<cfset NFCVertex = 30>
		<cfif NFCVertex gt 0>
			<cfset storeObject.source = NFCVertex - 1> <!--- Coldfusion Array is 1 based Index --->
			<cfset plotMap()>
		<cfelse>
			<cfset cfclient.device.notification.alert("Woops!","We do not recognize this tag!","Ok!")>
		</cfif> 
		<cfset cfclient.device.notification.vibrate(1000)>
	</cffunction>
		
	<cffunction access="public" name="tagDetectionStatusUpdate" returntype="void" >
		<cfargument name="result" required="no">
		<cfset document.getElementById('page2TagStatus').innerHTML = "Scan the nearest NFC tag to update directions...">
	</cffunction>
	
	<cffunction access="public" name="failureUpdate" returntype="void" >
		<cfargument name="reason" required="no">
		<cfset cfclient.device.notification.alert("Woops!","Something went wrong! Well, I cant debug myself :(","Oops!")>
		<cfset document.getElementById('page2TagStatus').innerHTML = "Something's Wrong!'">
	</cffunction>
</cfclient>