<cfclient>
	<cfset document.getElementById('tagStatus').innerHTML = "Updating Status..">
	<cfset addListeners()>
	<cfset document.getElementById('tagStatus').innerHTML = "Updating Status....">
	
	<cffunction access="public" name="tagDetected" returntype="void">
		<cfargument name="nfcEvent" required="yes">
		<!--- <cfset NFCString = JSON.stringify(nfc.bytesToHexString(nfcEvent.tag.id))>
		<cfset cfclient.device.notification.alert("Woops!",NFCString,"Oops!")>
		<cfset var dbServ = new server.webservice()>
		<cfset NFCVertex = dbServ.getVertexFromNFC(NFCString)>
		<cfset cfclient.device.notification.alert("Woops!",NFCVertex,"Oops!")> --->
		<cfset NFCVertex = 31>
		<cfif NFCVertex > 0>
			<cfset document.getElementById('tagStatus').innerHTML = "We found your location!">
			<cfset document.getElementById('tagData').innerHTML = "Now tell us what you are looking for,"><!---"Now tell us what you are looking for,"> --->
			<cfset storeObject.source = NFCVertex - 1> 
			<cfset document.getElementById('selectItem').css("visibility", "visible")>
			<cfset document.getElementById('searchLink').css("visibility", "visible")>
			<cfset cfclient.device.notification.vibrate(1000)>
		<cfelse>
			<cfset document.getElementById('tagStatus').innerHTML = "Please scan the nearest NFC tag...">
			<cfset document.getElementById('tagData').innerHTML = "We do not recognize this tag!">
			<cfset cfclient.device.notification.vibrate(1000)>
		</cfif> 
	</cffunction>
	
	<cffunction access="public" name="tagDetectionStatus" returntype="void" >
		<cfargument name="result" required="no">
		<cfset document.getElementById('tagStatus').innerHTML = "Please scan the nearest NFC tag...">
	</cffunction>
	
	<cffunction access="public" name="failure" returntype="void" >
		<cfargument name="reason" required="no">
		<cfset cfclient.device.notification.alert("Woops!","Something went wrong! Well, I cant debug myself :(","Oops!")>
		<cfset document.getElementById('tagStatus').innerHTML = "Something's Wrong!'">
	</cffunction>
</cfclient>