var storeObject = {
    source: 31,
    destination : 2,
    destinationString : "Chocolates",
	pathArray:null
}

var sourceDirection = 0;

$(document).on('pagebeforeshow', '#first', function(e, data){
	$("#tagStatus").html("Please scan the nearest NFC tag...");
	$("#tagData").html("");
	$('#selectItemContainer').css("visibility", "hidden");
	$('#searchLink').css("visibility", "hidden"); 
});

$(document).on('pagebeforehide', '#first', function(e, data){
	// HACK TO SHOW PROGRESS IN 'SOURCE VERTEX' OVER TIME - FOR DESKTOP DEMO ONLY - DISABLE THE BELOW CODE AFTER INTEGRATING SERVER
	/*storeObject.source--;
	if(storeObject.source < 1)
		storeObject.source = 30;*/
});

$(document).on('pagebeforeshow', '#second', function(e, data){
	plotMap();
});

$(document).on('pagebeforehide', '#second', function(e, data){
	loadDefaultsSecondScreen();
});

$(document).on('change','#selectItem',function(){
	storeObject.destination = $( "#selectItem" ).val();
    storeObject.destinationString = $( "#selectItem option:selected" ).text();
});

function loadDefaultsSecondScreen(){
	for(var i = 0; i < storeObject.PathArray.length-1; i++){
	    $("img[name*='v" + storeObject.PathArray[i] + " v" + storeObject.PathArray[i+1] + " ']").css("visibility", "hidden");
	}
	$("img[name*='r" + storeObject.destination + " ']").attr("style", "content:url('images/Rack.png')");
	
	storeObject.PathArray = null;
	
	$("#myLocation").offset({ top: 0, left: 0 });
}

function plotMap(){
	var source = storeObject.source; 
	var destination = storeObject.destination; 
	
	//Position around source
	var sourceAsString = source.toString();
	var queryConstruct = "img[name*='r" +sourceAsString + " ']";
	
	var coordinates = getCoordinates(source);
	$("#myLocation").offset({ top: coordinates.top, left: coordinates.left });
	
	// Set Destination Image
	var destinationImage = "";
	
	switch((parseInt(destination)+1)%4){
		case 1:
			// Top Left
			destinationImage = "images/RackTopLeft.png";
			break;
			
		case 2:
			// Top Right
			destinationImage = "images/RackTopRight.png";
			break;
			
		case 3:
			// Bottom Left
			destinationImage = "images/RackBottomLeft.png";
			break;
			
		case 0:
			// Bottom Right
			destinationImage = "images/RackBottomRight.png";
			break;
			
		default:
			destinationImage = "images/RackDestinate.png";
			break;
	}
	$("img[name*='r" + destination + " ']").attr("style", "content:url('"+destinationImage+"')");
	
	// Get Path from Source to Destination
	var Path = getPath(source, destination).toString();
	Path = source.toString() + "," + Path;
	
	$("#Message").html("Follow the directions below to find, <b><i>" + storeObject.destinationString + "</b></i>");
	
	storeObject.PathArray = Path.split(',');
	storeObject.PathArray.sort(function(a,b){return a - b});
	
	for(var i = 0; i < storeObject.PathArray.length-1; i++){
	    $("img[name*='v" + storeObject.PathArray[i] + " v" + storeObject.PathArray[i+1] + " ']").css("visibility", "visible");
	}
	
	drawCurrentLocation(0);
}

function getCoordinates(sourceVertex){
	var baseTop = 180;
	var baseLeft = 20;
	var rackWidth = 130;
	var rackHeight = 28;
	var verticalWidth = 34;
	var horizontalHeight = 17;
	
	var coordinates = {};
	
	var tempPositionInRack = (parseInt(sourceVertex)+1)%4; // 1,2,3, or 0
	if(tempPositionInRack == 0)
		tempPositionInRack = 4;
	
	var sourceVertexPositionInRack = tempPositionInRack - 1; // 0, 1,2 or 3
	
	var sourceVertexRack = Math.ceil((sourceVertex+1)/4); // 1 to 8
	
	var horizontalPositionOfRack = (sourceVertexRack-1)%2; // 0 or 1
	var verticalPositionOfRack = Math.ceil(sourceVertexRack/2) - 1; // 0 , 1 , 2, 3
	
	coordinates.top = baseTop + ((rackHeight + horizontalHeight) * verticalPositionOfRack) + (rackHeight * Math.floor(sourceVertexPositionInRack/2));
	coordinates.left = baseLeft + (horizontalPositionOfRack * (rackWidth + verticalWidth)) + (rackWidth * (sourceVertexPositionInRack%2));
	
	return coordinates;
}

function createSourcePointer(CompassDirection){
	var MapDirection = 0;
	
	directionToPlot = CompassDirection - sourceDirection;
	sourceDirection = CompassDirection;
	
	drawCurrentLocation(directionToPlot); // Match Map direction and detected degree from Compass
}

function drawCurrentLocation(angle) {
	 var myCanvas = document.getElementById('myLocation');
	 var ctx = myCanvas.getContext('2d');

 	// Clear the canvas
  	ctx.clearRect(0, 0, myCanvas.width, myCanvas.height);
	
  	// Move registration point to the center of the canvas
  	ctx.translate(myCanvas.width/2, myCanvas.width/2);
	ctx.rotate(angle*Math.PI / 180);
	// Move registration point back to the top left corner of canvas
	ctx.translate(-myCanvas.width/2, -myCanvas.width/2);
	
  	var img = new Image;
  	img.onload = function(){
  		ctx.drawImage(img,0,0);
  	};
  	img.src = "images/MyLoc.png";
}

// Takes two nodes as input, and gives back the path. Give Input, 32,11
function getPath(vSource, vDestination){
	var _ = Infinity;
	
	// Generate adjacency matrix
	var edges = new Array();
	for(var i = 0; i < 32; i++) {
		var rowItem = new Array();
		for(var j = 0; j < 32; j++){
			 rowItem.push(_);
		}
		edges.push(rowItem);
	}
	
	// Populate adjacency matrix with valid edges
	
	var rows = 4;
	var racks = 8;
	
	for(var k=0; k < rows; k++) {
	
	try {
		edges[0+k*racks][(0+k*racks) + 1] = 3;
		edges[0+k*racks][(0+k*racks) + 2] = 1;
		edges[0+k*racks][(0+k*racks) - 6] = 2;
		
		edges[1+k*racks][(1+k*racks) - 1] = 3;
		edges[1+k*racks][(1+k*racks) + 2] = 1;
		edges[1+k*racks][(1+k*racks) + 3] = 2;
		edges[1+k*racks][(1+k*racks) - 6] = 2;
		
		edges[2+k*racks][(2+k*racks) - 2] = 1;
		edges[2+k*racks][(2+k*racks) + 1] = 3;
		edges[2+k*racks][(2+k*racks) + 6] = 2;
		
		edges[3+k*racks][(3+k*racks) - 2] = 1;
		edges[3+k*racks][(3+k*racks) - 1] = 3;
		edges[3+k*racks][(3+k*racks) + 3] = 2;
		edges[3+k*racks][(3+k*racks) + 6] = 2;
		
		edges[4+k*racks][(4+k*racks) + 1] = 3;
		edges[4+k*racks][(4+k*racks) + 2] = 1;
		edges[4+k*racks][(4+k*racks) - 3] = 2;
		edges[4+k*racks][(4+k*racks) - 6] = 2;
		
		edges[5+k*racks][(5+k*racks) - 1] = 3;
		edges[5+k*racks][(5+k*racks) + 2] = 1;
		edges[5+k*racks][(5+k*racks) - 6] = 2;
		
		edges[6+k*racks][(6+k*racks) - 2] = 1;
		edges[6+k*racks][(6+k*racks) + 1] = 3;
		edges[6+k*racks][(6+k*racks) - 3] = 2;
		edges[6+k*racks][(6+k*racks) + 6] = 2;
		
		edges[7+k*racks][(7+k*racks) - 2] = 1;
		edges[7+k*racks][(7+k*racks) - 1] = 3;
		edges[7+k*racks][(7+k*racks) + 6] = 2;
	} catch(err){
		alert("Failed on index, " + k);
	}
}
	
	/* // Adjacency Martix Checker
	var sometext = "";
	$.each(edges , function( index, obj ) {
		sometext = sometext + index;
	    $.each(obj, function( key, value ) {
	        if(value == 1 || value == 2 || value == 3)
	        	sometext = sometext + " ["+key+"]"  + "["+value+"], " ;
	    });
	    sometext = sometext + "</br>"
	});
	$("#Message").html(sometext);*/
	
	// Compute the shortest paths from source to each other vertex in the graph.
	var shortestPathInfo = shortestPath(edges, 32, vSource);
	
	// Get the shortest path from source to destination
	var path = constructPath(shortestPathInfo, vDestination);
	
	//alert(path);
	return path;
}

// Source - http://mcc.id.au/2004/10/dijkstra.js
function shortestPath(edges, numVertices, startVertex) {
  var done = new Array(numVertices);
  done[startVertex] = true;
  var pathLengths = new Array(numVertices);
  var predecessors = new Array(numVertices);
  for (var i = 0; i < numVertices; i++) {
  	pathLengths[i] = edges[startVertex][i];
    if (edges[startVertex][i] != Infinity) {
      predecessors[i] = startVertex;
    }
  }
  pathLengths[startVertex] = 0;
  for (var i = 0; i < numVertices - 1; i++) {
    var closest = -1;
    var closestDistance = Infinity;
    for (var j = 0; j < numVertices; j++) {
      if (!done[j] && pathLengths[j] < closestDistance) {
        closestDistance = pathLengths[j];
        closest = j;
      }
    }
    done[closest] = true;
    
    for (var j = 0; j < numVertices; j++) {
      if (!done[j]) {
      	try {
	      	var possiblyCloserDistance = pathLengths[closest] + edges[closest][j];
	        if (possiblyCloserDistance < pathLengths[j]) {
	          pathLengths[j] = possiblyCloserDistance;
	          predecessors[j] = closest;
	        }
	  	}
	    catch(errr){
	    	alert("Error at, pathLengths[" + closest + "] edges["  +  closest + "] [" + j + "]");
	    }
      }
    }
  }
  return { "startVertex": startVertex,
           "pathLengths": pathLengths,
           "predecessors": predecessors };
}

function constructPath(shortestPathInfo, endVertex) {
  var path = [];
  while (endVertex != shortestPathInfo.startVertex) {
    path.unshift(endVertex);
    endVertex = shortestPathInfo.predecessors[endVertex];
  }
  return path;
}

function addListeners(){
	// Add listeners for NFC - Support not available in PhoneGap
	nfc.addNdefListener(tagDetected,tagDetectionStatus,failure);
	nfc.addTagDiscoveredListener(tagDetected,tagDetectionStatus,failure);
	nfc.addMimeTypeListener('text/pg', tagDetected,tagDetectionStatus, failure);
}

function addListenersUpdate(){
	// Add listeners for NFC - Support not available in PhoneGap
	nfc.addNdefListener(tagDetectedUpdate,tagDetectionStatusUpdate,failureUpdate);
	nfc.addTagDiscoveredListener(tagDetectedUpdate,tagDetectionStatusUpdate,failureUpdate);
	nfc.addMimeTypeListener('text/pg', tagDetectedUpdate,tagDetectionStatusUpdate, failureUpdate);

	// Add listeners for Compass
	var compassOptions = { frequency: 500 };
	var watchID = navigator.compass.watchHeading(compassDetectionStatus, failure, compassOptions);
	//Call navigator.compass.clearWatch(watchID); on Destruct
}