<cfcomponent client="true">
	
	<cffunction access="remote" name="getVertexFromNFC" returntype="numeric">
			<cfargument name="NFC" required="yes">
			
			<cfset TagArray = arrayNew(1)>
			<cfset TagArray[32] = "7ddba72c"/> <!--- INOEL --->
			<cfset TagArray[31] = "2d691ac0"/> <!--- YRR --->
			<cfset TagArray[30] = "ac69eed5sd"/> <!--- PNAYAK --->
			<cfset TagArray[29] = '"7ddba72c"'/>
			<cfset TagArray[28] = "SampleTag"/>
			<cfset TagArray[27] = "SampleTag"/>
			<cfset TagArray[26] = "SampleTag"/>
			<cfset TagArray[25] = "SampleTag"/>
			<cfset TagArray[24] = "SampleTag"/>
			<cfset TagArray[23] = "SampleTag"/>
			<cfset TagArray[22] = "SampleTag"/>
			<cfset TagArray[21] = "SampleTag"/>
			<cfset TagArray[20] = "SampleTag"/>
			<cfset TagArray[19] = "ac69eed5"/>
			<cfset TagArray[18] = "SampleTag"/>
			<cfset TagArray[17] = "SampleTag"/>
			<cfset TagArray[16] = "SampleTag"/>
			<cfset TagArray[15] = "SampleTag"/>
			<cfset TagArray[14] = "SampleTag"/>
			<cfset TagArray[13] = "SampleTag"/>
			<cfset TagArray[12] = "SampleTag"/>
			<cfset TagArray[11] = "SampleTag"/>
			<cfset TagArray[10] = "SampleTag"/>
			<cfset TagArray[9] = "SampleTag"/>
			<cfset TagArray[8] = "SampleTag"/>
			<cfset TagArray[7] = "SampleTag"/>
			<cfset TagArray[6] = "SampleTag"/>
			<cfset TagArray[5] = "SampleTag"/>
			<cfset TagArray[4] = "SampleTag"/>
			<cfset TagArray[3] = "SampleTag"/>
			<cfset TagArray[2] = "SampleTag"/>
			<cfset TagArray[1] = "SampleTag2"/>
			
			<cfreturn #arrayFind( TagArray, NFC)#>
	</cffunction>

	<cffunction access="remote" name="getCommodities" returntype="array">
			<cfset commoditiesArray = arrayNew(1)>
			<cfset #commoditiesArray[1]# = {vertex:"2",item:"Chocolates"}/>
			<cfset #commoditiesArray[2]# = {vertex:"4",item:"Sweets"}/>
			<cfset #commoditiesArray[3]# = {vertex:"9",item:"Bread"}/>
			<cfset #commoditiesArray[4]# = {vertex:"15",item:"Grocery"}/>
			<cfset #commoditiesArray[5]# = {vertex:"18",item:"KitchenWare"}/>
			<cfset #commoditiesArray[6]# = {vertex:"21",item:"Toys"}/>
			<cfset #commoditiesArray[7]# = {vertex:"22",item:"Gifts"}/>
			<cfreturn commoditiesArray>
	</cffunction>

</cfcomponent>