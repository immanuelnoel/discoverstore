<!DOCTYPE html>
<html> 
	<head> 
		<title>Discover Store</title> 

		<!--- loading jquery mobile scripts and styles --->
		<link rel="stylesheet" href="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.css" />
		<script src="http://code.jquery.com/jquery-1.8.2.min.js"></script>
		<script src="http://code.jquery.com/mobile/1.3.0/jquery.mobile-1.3.0.min.js"></script>

		<link href='http://fonts.googleapis.com/css?family=PT+Serif+Caption:400,400italic' rel='stylesheet' type='text/css'>
		<link rel="stylesheet" href="themes/android-theme.css" />
		<link rel="stylesheet" href="css/main.css" />
		
		<script src="js/main.js"></script>
		
		<script type="text/javascript">
			var DEBUG_ANDROID_THEME=true
		</script>

	</head>
	<body> 

		<!--- loading DNFI plugins and device detection plugins --->
	 	<cfclientsettings enableDeviceAPI=true>
	 	<cfClientSettings detectDevice=true />
	 
		
	 	<!--- starting page for app --->
		<div data-role="page" id="first" data-cache="true">
			
			<!--- page header --->
			<div data-role="header" data-position="fixed" data-theme="a" data-tap-toggle="false" >
				<h1>Discover Store</h1>
			</div>
			<!--- /header --->
			
			<!--- main content of the page --->
			<div id="div_content" data-role="content" >
				<img src="images/Logo.png" width="64" height="64" style="float:right;">
				<h4 id="tagStatus">Updating Status</h4>
				<h4 id="tagData"></h4>
				
				<cfclient>
					<cfset document.getElementById('tagStatus').innerHTML = "Updating Status..">
					<cfset addListeners()>
					<cfset document.getElementById('tagStatus').innerHTML = "Updating Status....">
					
					<cffunction access="public" name="tagDetected" returntype="void">
						<cfargument name="nfcEvent" required="yes">
						<cfset NFCString = nfc.bytesToHexString(nfcEvent.tag.id)>
						<cfset obj = CreateObject("component", "webservice")>
						<cfset NFCVertex = obj.getVertexFromNFC(NFCString)>
						<cfif NFCVertex GT 0>
							<cfset document.getElementById('tagStatus').innerHTML = "We found your location!">
							<cfset document.getElementById('tagData').innerHTML = "Now tell us what you are looking for,">
							<cfset storeObject.source = NFCVertex - 1> 
							<cfset document.getElementById('selectItemContainer').style.visibility = "visible">
							<cfset document.getElementById('searchLink').style.visibility = "visible">
						<cfelse>
							<cfset document.getElementById('tagStatus').innerHTML = "Please scan the nearest NFC tag...">
							<cfset document.getElementById('tagData').innerHTML = "We do not recognize this tag!">
						</cfif> 
						<cfset cfclient.device.notification.vibrate(1000)>
					</cffunction>
					
					<cffunction access="public" name="tagDetectionStatus" returntype="void" >
						<cfargument name="result" required="no">
						<cfset document.getElementById('tagStatus').innerHTML = "Please scan the nearest NFC tag...">
					</cffunction>
					
					<cffunction access="public" name="failure" returntype="void" >
						<cfargument name="reason" required="no">
						<cfset cfclient.device.notification.alert("Woops!","Something went wrong! Well, I cant debug myself :(","Oops!")>
						<cfset document.getElementById('tagStatus').innerHTML = "Something's Wrong!'">
					</cffunction>
					
					<div id="selectItemContainer" class="selectItemContainerClass" data-role="fieldcontain">
					<select id="selectItem" data-theme="b" data-overlay-theme="d" data-native-menu="false">
						<cfset dbServ = CreateObject("component", "webservice")>
						<cfset commodities = dbServ.getCommodities() >
						<cfloop index="i" from="1" to="#arrayLen(commodities)#">
							<cfoutput>
								<option value="#commodities[i].vertex#">#commodities[i].item#</option>
							</cfoutput>
						</cfloop>
					</select>
				</div>
					
				</cfclient>
				
				
				
				<a id="searchLink" href="#second" data-role="button" data-transition="slide" data-theme="b">Search</a>					
																																												
			</div>
			
			<div data-role="footer" data-position="fixed" data-theme="a" data-tap-toggle="false" >
				<h4>The Wanderers!</h4>
			</div>
		</div>
		<div data-role="page" id="second" data-cache="true">
			<div data-role="header" data-position="fixed" data-theme="a" data-tap-toggle="false" >
				<a href="#first" data-icon="back" data-theme="b">Back</a>
				<h1>Discover Store</h1>
			</div>
			
			<div id="div_content" data-role="content" class="overriddenContainer">
				
				<div class="outerContent">
					<p id="Message"><i>Follow the directions below to find, <b>Chocolates!</b></i></p>
					<p id="page2TagStatus">Updating Status...</p>
				</div>

				<div class="mainLayoutContainer">
					<div class="HorizontalContainer-partition">
						<img name="v0 v1 " class="HorizontalContainer-horizontalLeft">
						<img name="v4 v5 " class="HorizontalContainer-horizontalRight">
					</div>
					<div id="rackSet1" class="HorizontalContainer-Rack">
						<img name="v0 v2 " class="HorizontalContainer-leftLine">
						<img name="r0 r1 r2 r3 " class="Rack">
						<img name="v1 v3 v4 v6 " class="HorizontalContainer-centerLine">
						<img name="r4 r5 r6 r7 " class="Rack">
						<img name="v5 v7 " class="HorizontalContainer-rightLine">
					</div>
					<div class="HorizontalContainer-partition">
						<img name="v2 v3 v8 v9 " class="HorizontalContainer-horizontalLeft">
						<img name="v6 v7 v12 v13 " class="HorizontalContainer-horizontalRight">
					</div>
					<div id="rackSet2" class="HorizontalContainer-Rack">
						<img name="v8 v10 " class="HorizontalContainer-leftLine">
						<img name="r8 r9 r10 r11 " class="Rack">
						<img name="v9 v11 v12 v14 " class="HorizontalContainer-centerLine">
						<img name="r12 r13 r14 r15 " class="Rack">
						<img name="v13 v15 " class="HorizontalContainer-rightLine">
					</div>
					<div class="HorizontalContainer-partition">
						<img name="v10 v11 v16 v17 " class="HorizontalContainer-horizontalLeft">
						<img name="v14 v15 v20 v21 " class="HorizontalContainer-horizontalRight">
					</div>
					<div id="rackSet3" class="HorizontalContainer-Rack">
						<img name="v16 v18 " class="HorizontalContainer-leftLine">
						<img name="r16 r17 r18 r19 " class="Rack">
						<img name="v17 v19 v20 v22 " class="HorizontalContainer-centerLine">
						<img name="r20 r21 r22 r23 " class="Rack">
						<img name="v21 v23 " class="HorizontalContainer-rightLine">
					</div>
					<div class="HorizontalContainer-partition">
						<img name="v18 v19 v24 v25 " class="HorizontalContainer-horizontalLeft">
						<img name="v22 v23 v28 v29 " class="HorizontalContainer-horizontalRight">
					</div>
					<div id="rackSet4" class="HorizontalContainer-Rack">
						<img name="v24 v26 " class="HorizontalContainer-leftLine">
						<img name="r24 r25 r26 r27 " class="Rack">
						<img name="v25 v27 v28 v30 " class="HorizontalContainer-centerLine">
						<img name="r28 r29 r30 r31 " class="Rack">
						<img name="v29 v31 " class="HorizontalContainer-rightLine">
					</div>
					<div class="HorizontalContainer-partition">
						<img name="v26 v27 " class="HorizontalContainer-horizontalLeft">
						<img name="v30 v31 " class="HorizontalContainer-horizontalRight">
					</div>
				</div>
				
				<cfclient>
					<cfset addListenersUpdate()>
					
					<cffunction access="public" name="compassDetectionStatus" returntype="void" >
						<cfargument name="result" required="yes">
						<cfset createSourcePointer(#result.magneticHeading#)>
					</cffunction>
					
					<cffunction access="public" name="tagDetectedUpdate" returntype="void">
						<cfargument name="nfcEvent" required="yes">
						<cfset NFCString = nfc.bytesToHexString(nfcEvent.tag.id)>
						<cfset obj = CreateObject("component", "webservice")>
						<cfset NFCVertex = obj.getVertexFromNFC(NFCString)>
						<cfif NFCVertex gt 0>
							<cfset storeObject.source = NFCVertex - 1> <!--- Coldfusion Array is 1 based Index --->
							<cfset loadDefaultsSecondScreen()>
							<cfset plotMap()>
						<cfelse>
							<cfset cfclient.device.notification.alert("Woops!","We do not recognize this tag!","Ok!")>
						</cfif> 
						<cfset cfclient.device.notification.vibrate(1000)>
					</cffunction>
						
					<cffunction access="public" name="tagDetectionStatusUpdate" returntype="void" >
						<cfargument name="result" required="no">
						<cfset document.getElementById('page2TagStatus').innerHTML = "Scan the nearest NFC tag to update directions...">
					</cffunction>
					
					<cffunction access="public" name="failureUpdate" returntype="void" >
						<cfargument name="reason" required="no">
						<cfset cfclient.device.notification.alert("Woops!","Something went wrong! Well, I cant debug myself :(","Oops!")>
						<cfset document.getElementById('page2TagStatus').innerHTML = "Something's Wrong!'">
					</cffunction>
				</cfclient>
				
				<Canvas id="myLocation" width="45" height="45" style="position:absolute;">
			</div>
			
			<div data-role="footer" data-position="fixed" data-theme="a" data-tap-toggle="false" >
				<h4>The Wanderers!</h4>
			</div>
		</div>
	</body>
</html>
